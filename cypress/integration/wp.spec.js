/**
 * @desc Prefill form field based on cookie data
 */
 const args = process.argv.slice(2)[0];

describe('Prefill form', () => {
    beforeEach(function () {
        cy.setCookie('wordpress_test_cookie', 'WP+Cookie+check');
    });

    it('Forgot password', () => {
        cy.visit(Cypress.env('host'));
        cy.get('#user_login').type(Cypress.env('login'))
        cy.get('#wp-submit').click();
        cy.url().should('include', '?checkemail=confirm');
    });
});